// app.js

new Vue({
  el: '#albums',

  // Here we can register any values or collections that hold data
  // for the application
  data: {
    search: { artist: '', album: '', track: '' },
    track: { tracknumber: '', title: '', id: '', artist: '' },
    tracks: [],
    album: { id: '', name: '', year: '', arturl: '' },
    albums: []
  },

  // Anything within the ready function will run when the application loads
  ready: function() {
    //var VueAlert = require('vue-alert');
    //Vue.use(VueAlert);
    // When the application loads, we want to call the method that initializes
    // some data
    //this.fetchEvents();
  },

  // Methods we want to use in our application are registered here
  methods: {
    // We dedicate a method to retrieving and setting some data
    //fetchEvents: function() {
    //  var events = [
    //  ];
      // $set is a convenience method provided by Vue that is similar to pushing
      // data onto an array
    //  this.$set('events', events);
    //},

    // Adds an event to the existing events array
    searchButton: function() {
      if(this.search.artist) {
        if(!this.search.album) {
          if(!this.search.track) {
            // Artist, No album, No Track -> show discography
            this.$http.get('http://playlist:9999/search_id', {params: {
              type: 'artist',
              artist: this.search.artist,
              exact: 'no'
            }}).then((response) => {
              // Yay we got something back from the GMusicProxy
              // confirm(response.text());
              this.getDiscography(response.text());
            });
          } else {
            // Artist, No album, Track - search for the track
            this.$http.get('http://playlist:9999/get_by_search', {params: {
              type: 'matches',
              artist: this.search.artist,
              title: this.search.track,
              exact: 'no'
            }}).then((response) => {
              var playlist = M3U.parse(response.text());
              var tracklist = [];
               for(var i=0, len = playlist.length; i < len; i++) {
                 playlist[i]['file'] = String(playlist[i]['file']).replace( /.*id=/ , "" ); // strip out the http:// preamble
                 tracklist[i] = { tracknumber: i, title: playlist[i]['title'], id: playlist[i]['file'], artist: playlist[i]['artist'] };
               }

               // confirm(JSON.stringify(playlist));
               this.$set('tracks', tracklist);
            });
          }
        } else {
          // Artist, Album, Ignore any track
          this.$http.get('http://playlist:9999/search_id', {params: {
            type: 'album',
            artist: this.search.artist,
            title: this.search.album,
            exact: 'no'
          }}).then((response) => {
            // We got a response!
            this.selectAlbum({id: response.text()});
          });
        }
      }
    },

    getDiscography: function(artist_id) {
      this.$http.get('http://playlist:9999/get_discography_artist', {params: {
        id: artist_id,
        format: 'text'
      }}).then((response) => {
        // We got a response!
        var albumarray = response.text().split("\n");
        for(var i=0, len = albumarray.length; i < len; i++) {
          var thisalbum = albumarray[i].split("|");
          thisalbum[2] = String(thisalbum[2]).replace( /.*id=/ , "" ); // strip out the http:// preamble
          albumarray[i] = { name: thisalbum[0], arturl: '', year: thisalbum[1], id: thisalbum[2] };
        }
        this.$set('albums', albumarray);
      });
    },

    selectAlbum: function(album) {
      this.$set('albums', []);
      this.$http.get('http://playlist:9999/get_album', {params: {
        id: album.id
      }}).then((response) => {
        var playlist = M3U.parse(response.text());
        var tracklist = [];
        for(var i=0, len = playlist.length; i < len; i++) {
          playlist[i]['file'] = String(playlist[i]['file']).replace( /.*id=/ , "" ); // strip out the http:// preamble
          tracklist[i] = { tracknumber: i, title: playlist[i]['title'], id: playlist[i]['file'], artist: playlist[i]['artist'] };
        }

        // confirm(JSON.stringify(playlist));
        this.$set('tracks', tracklist);
      });
    },

    selectTrack: function(track) {
      this.$set('tracks', []);
      this.$http.get('http://10.0.0.252/status.html', {params: {
        player: 'b8:27:eb:11:09:95',
        p0: 'playlist',
        p1: 'add',
        p2: 'http://playlist:9999/get_song?id=' + track.id,
        p3: track.artist + ' /// ' + track.title
      }});
      prompt('Track added!', 'wget -O "' + track.artist + ' - ' + track.title + '.mp3" http://playlist:9999/get_song?id=' + track.id);
      //alert("Track added!");
      //this.$alert('Track has been added.');
    }

  }

});
